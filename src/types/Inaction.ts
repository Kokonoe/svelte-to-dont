export interface Inaction {
    title: string;
    dueDate: Date;
    notes: string;
}
